long long fatorial(long long n)
{
	// Implemente o fatorial aqui
	long long fatorial= n;
	while (n != 1) {
		n-=1;
		fatorial*= n;
	}
	return fatorial;
}

int main(int argc, char **argv, char **envp)
{
	if(argc > 1)
	{
		printf("%lld\n", fatorial(atoll(argv[1])));
	}
	else
	{
		puts("Voce precisa dar um valor para n!");
	}
}
